package com.sapient.restaurant.Zomato;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class check extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String usernameString = request.getParameter("username");
		String passwordString = request.getParameter("password");
		//implement authentication
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("home.jsp");
		request.setAttribute("usernameString", usernameString);
		requestDispatcher.forward(request, response);
	}

}
