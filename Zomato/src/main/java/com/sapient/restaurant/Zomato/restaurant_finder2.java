package com.sapient.restaurant.Zomato;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class restaurant_finder2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		
		
		JsonParser parser = new JsonParser();
        Object obj = parser.parse((String) request.getAttribute("json"));
        JsonObject jb = (JsonObject) obj;

        //now read
        JsonArray jsonObject1 =  (JsonArray) jb.get("results");
        JsonObject jsonObject2 = (JsonObject) jsonObject1.get(0);
        JsonObject jsonObject3 = (JsonObject) jsonObject2.get("geometry");
        JsonObject location =  (JsonObject) jsonObject3.get("location");
        String latitudeString = location.get("lat").toString();
        String longitudeString = location.get("lng").toString();
        
        
        
        
        
        String zomatokeyString = "2a2ed48817cb491e7da3021993e47d6c";
        String urlString = "https://developers.zomato.com/api/v2.1/geocode?apikey=" + zomatokeyString + "&lat=" + latitudeString + "&lon=" + longitudeString;
        
        HttpGet getRequest = new HttpGet(urlString);
		getRequest.addHeader("accept", "application/json");
		HttpResponse responseReceived = httpClient.execute(getRequest);
		int statusCode = responseReceived.getStatusLine().getStatusCode();
        if (statusCode != 200)
        {
            throw new RuntimeException("Failed with HTTP error code : " + statusCode);
        } 
        HttpEntity httpEntity = responseReceived.getEntity();
        String apiOutput2 = EntityUtils.toString(httpEntity);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("home.jsp");
        request.setAttribute("restaurants", apiOutput2);
        requestDispatcher.forward(request, response);
	}

}
